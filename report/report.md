# Progetto di gruppo - esame di Programmazione di Reti

A.A. 2017/2018

Professore: Giovanni Pau

Studenti:
- Andrea Casadei \<andrea.casadei22@studio.unibo.it\>
- Fabrizio Margotta \<fabrizio.margotta@studio.unibo.it\>

## Indice

0. Introduzione
1. Analisi
2. Design
3. Sviluppo
4. Commenti finali
5. Appendice A: Guida utente
6. Appendice B: Introduzione a NDN
7. Apendice C: Il simulatore ndnSIM

## 0. Introduzione

L'obiettivo della seguente relazione di progetto è quello di presentare il
lavoro svolto dal gruppo nell'approfondimento e nella programmazione di reti
basate sul modello architetturale "Named Data Networking" (NDN).

Si precisa che tale modello è tutt'ora in fase di ricerca accademica e molte
delle sue componenti sono altamente sperimentali.

Il contenuto di tale relazione potrebbe pertanto essere deprecato.

Si allega al documento una serie di Appendici, tra cui è presente una
introduzione a NDN e ai suoi concetti di base. Tale elaborato può risultare
utile ai non addetti ai lavori nel comprendere il contenuto stesso della
relazione.

## 1. Analisi

Il gruppo di lavoro ha collaborato alla realizzazione di uno scenario mirato ad
approfondire la programmazione di una rete basata su NDN. In particolar modo
l'approfondimento verte sull'analisi e la programmazione dei componenti Content 
Store e Pending Interest Table, con specifico riferimento al multipath
forwarding.

### Scenario: simulato vs fisico

Tali scenari di programmazione di reti possono essere realizzati, a livelli più
o meno avanzati, in contesti fisici o tramite appositi simulatori.

NDN permette di utilizzare sia un testbed fisico compatibile con l'attuale
connessione Internet basata su IP, sia un simulatore NDN puro.

Il gruppo di lavoro, dovendo unicamente realizzare uno scenario che
approfondisse caratteristiche strutturali di NDN, ha optato per il simulatore
"ndnSIM".  

## 2. Design

### Topologia della rete

Lo scenario è volutamente semplice e coinvolge un ristretto numero di host,
poiché non è obiettivo di questo progetto analizzare la comunicazione in NDN da
un punto di vista prestazionale.

Siano 1 Producer, 3 Router e 9 Consumer.

Si ipotizzano due edifici collegati ad un router che a sua volta li connette
alla rete pubblica:
- l'edificio A ha un solo Consumer attivo
- l'edificio B ha 8 Consumer attivi distribuiti su due piani
- ogni piano dell'edificio B ha un router che collega i 4 Consumer in esso
  situati

È possibile osservare il funzionamento di CS e PIT con topologie ben più
semplici di quella appena descritta, tuttavia in questo modo è possibile
comprendere modo come lavorano tali elementi in reti locali e non.

### Simulazione

Tutti i Consumer hanno come Interest il Nome `/prefix/1`.

Il Producer risponde regolarmente alle richieste effettuate ed i router sono
inizializzati con le cache vuote.

Nell'edificio B, 3 consumer del piano 1 eseguono **contemporaneamente** la
richiesta, che viene prontamente inviata al Producer.  
Quest'ultimo provvede dunque alla creazione e all'invio del relativo Data, che 
viene correttamente ricevuto dai Consumer.

![](./img/topology/out/26_s.png)

Successivamente, il Consumer dello stesso piano dell'edificio B effettua la
medesima richiesta.

![](./img/topology/out/5_s.png)

In seguito, il Consumer dell'edificio A manda un Interest per `/prefix/1`.

![](./img/topology/out/8_s.png)

Infine, tutti i Consumer al secondo piano dell'edificio B eseguono la stessa
richiesta.

![](./img/topology/out/13_s.png)

Dai Router ci si attende dunque che i contenuti già serviti vengano reperiti
dal Content Store  e che i contenuti ancora non serviti che hanno richieste
multiple in attesa, vengano soddisfatti in un sol colpo (multipath forwarding).

### Applicazioni

Sono state utilizzate implementazioni già esistenti di Consumer e Producer: si è
fatto uso di helper forniti dalle API del simulatore.

Per i Consumer è stato scelto `ConsumerBatches`, che permette di inviare un
numero di Interest (diversi, con numero in ordine crescente) ad intervalli di
tempo definiti dall'utente.

Per i Producer è stato individuato `Producer`, che risponde con il Data per
l'esatta richiesta effettuata.

## 3. Sviluppo

### Workflow con ndnSIM

0. Preparazione dell'ambiente di sviluppo
1. Definire la topologia
2. Configurare CS
3. Installare lo stack NDN sui nodi
4. Configurare FIB
5. Installare una forwarding strategy
6. Avviare la simulazione (opzionale: con visualizer)
7. Collezionare metriche, statistiche e log

### 3.0 Preparazione dell'ambiente di sviluppo

Il simulatore ndnSIM viene installato in una macchina Docker basata su Ubuntu
16.04.

Le istruzioni di installazione sono reperibili sul sito
[ndnsim.net](https://ndnsim.net).

Lo scenario viene invece sviluppato e fornito in un repository separato da
quello contenente il simulatore.

Un'altra buona pratica consiste nello specificare i commit dei repository
utilizzati, che sono riportati di seguito (e nel `Dockerfile`), così da poter
replicare senza problemi lo scenario ideato:
- `ns-3`: `05158549275e897c7d17922bdeada483b2b8ab11`
- `pybindgen`: `0466ae3508076ccfb803a16c0eeffc2ed3fc32c3`
- `ndnSIM`: `65c490630e1a2e9cdebaf505de8729838c000821`
- `NFD`: `3bebd1190b1c45f8acaa0fe1d3a3100651a062e4`
- `ndn-cxx`: `e1ae096efd8ad503ce7dbd616ee174afaed6c66b`

### 3.1 Definire la topologia

I Router, Producer e Consumer sono definiti nell'apposito file di testo,
opportunamente importato nel codice sorgente della simulazione.

Si ricorda che la prima sezione (`router`) contiene gli host e la loro
posizione.
La sezione `link` descrive i collegamenti e le metriche tra ogni host.

Può essere definita direttamente nel codice sorgente della simulazione, ma non è
una buona pratica.

Di seguito un estratto del file:

```ascii
# project-topology.txt

# router section defines topology nodes and their relative positions 
    (e.g., to use in visualizer)
router

# node  comment     yPos    xPos
Con0    NA          1       1
[...]
Con21   NA          4       13
Rtr1    NA          2       2
[...]
Rtr7    NA          5       12
Pro0    NA          6       8

# link section defines point-to-point links between nodes and
    characteristics of these links
link

# srcRtr   dstRtr     bandwidth   metric  delay   queue
Con0        Rtr1       1Mbps       1       10ms    10
[...]
Rtr7        Pro0       1Mbps       1       10ms    10
```

Si noti che la presenza di un numero di nodi superiore a quelli precedentemente
dichiarati è motivata da possibili usi futuri della topologia appena mostrata.

Di seguito sono riportate le istruzioni necessarie per la lettura del file di
testo all'interno del codice C++ della simualazione.

```c++
      /* Reading Topology */
      AnnotatedTopologyReader topologyReader("", 25);
      topologyReader.SetFileName("./scenarios/project-topology.txt");
      topologyReader.Read();
```

Si noti che anche chiamando i file `Con`, `Prod`, `Rtr` facendo rispettivamente
riferimento a Consumer, Producer, Router, essi non sono ancora effettivamente
riconosciuti come tali.
Più avanti, nel codice sorgente della simulazione, verrà effettuata tale operazione.
A tal proposito si rimanda al paragrafo 4.3.

### 3.2 Configurazione CS

### 3.3 Installazione stack NDN sui nodi

L'installazione dello stack NDN nei nodi desiderati (nel nostro caso tutti) è
fondamentale per poter abilitare la comunicazione basata su NDN stesso.

```c++
      /* Install NDN stack on all nodes */
      ndn::StackHelper ndnHelper;
      ndnHelper.InstallAll();
```

In seguito va definito il ruolo per ogni nodo, ovvero è necessario specificare
chi è da identificarsi come _Consumer_ e come _Producer_.

```c++
      /* Producer node */
      Ptr<Node> producer = Names::Find<Node>("Pro0");

      /* Consumer nodes */
      NodeContainer coBuildingA;
      NodeContainer coBuildingBFloor1;
      NodeContainer coBuildingBFloor2;
      coBuildingA.Add(Names::Find<Node>("Con16"));
      coBuildingBFloor1.Add(Names::Find<Node>("Con1"));
      coBuildingBFloor1.Add(Names::Find<Node>("Con8"));
      coBuildingBFloor1.Add(Names::Find<Node>("Con9"));
      coBuildingBFloor2.Add(Names::Find<Node>("Con2"));
      coBuildingBFloor2.Add(Names::Find<Node>("Con3"));
      coBuildingBFloor2.Add(Names::Find<Node>("Con10"));
      coBuildingBFloor2.Add(Names::Find<Node>("Con11"));
```

#### 3.3.1 Installazione applicazioni Consumer

Successivamente è necessario installare un'applicazione (_helper_) nei
Consumer.  
Specifichiamo inoltre il prefisso del _Nome_ trasmesso dai Consumer all'atto di
invio di un Interest.

L'helper scelto è _ConsumerBatches_, che permette di inviare un numero arbitrario di
Interest dopo un tempo _T_ dall'avvio della simulazione.  
Tale comportamento è estremamente utile nello scenario ideato, poiché consente
di installare la stessa strategia nei diversi piani ed edifici.  
È di fondamentale importanza, durante l'installazione nei nodi, specificare
tempistiche differenti, poiché è determinante nella buona riuscita dello
scenario ideato.

```c++
	  /* Set prefix */
      std::string prefix = "/prefix";
      ndn::AppHelper consumerHelper("ns3::ndn::ConsumerBatches");
      consumerHelper.SetPrefix(prefix);

      /* Set time strategy on Consumer nodes using Batches */
      consumerHelper.SetAttribute("LifeTime", StringValue("5s"));
      consumerHelper.SetAttribute("Batches", StringValue("1s 1"));
      consumerHelper.Install(coBuildingBFloor1);
      consumerHelper.SetAttribute("Batches", StringValue("3s 1"));
      consumerHelper.Install(Names::Find<Node>("Con0"));
      consumerHelper.SetAttribute("Batches", StringValue("4s 1"));
      consumerHelper.Install(coBuildingA);
      consumerHelper.SetAttribute("Batches", StringValue("5s 1"));
      consumerHelper.Install(coBuildingBFloor2);
```

#### 3.3.2 Installazione applicazioni Producer

Allo stesso modo installiamo un _helper_ nell'unico Producer ospitato dal nostro
scenario.  
Il comportamento di tale applicativo è molto semplice, in quanto risponde
all'Interest richiesto con un pacchetto _Data_ della dimensione (virtuale)
indicata.

```c++
      /* Install NDN applications on Producer */
      ndn::AppHelper producerHelper("ns3::ndn::Producer");
      producerHelper.SetPrefix(prefix);
      producerHelper.SetAttribute("PayloadSize", StringValue("1024"));
      producerHelper.Install(producer);
```

### 3.4 Configurazione FIB

Utilizziamo il componente `GlobalRoutingHelper` per calcolare automaticamente
routes e Forwarding Information Base.

```c++
      /* Installing global routing interface on all nodes */
      ndn::GlobalRoutingHelper ndnGlobalRoutingHelper;
      ndnGlobalRoutingHelper.InstallAll();

      /* Add `/prefix` origins to ndn::GlobalRouter */
      ndnGlobalRoutingHelper.AddOrigins(prefix, producer);

      /* Calculate and install FIBs */
      ndn::GlobalRoutingHelper::CalculateRoutes();
```

### 3.5 Installazione Forwarding Strategy

Come forwarding strategy è stata scelta _Best Strategy_ poiché inoltra gli
Interest al nodo successivo con costo inferiore.

L'installazione della forwarding strategy avviene mediante `StrategyChoiceHelper`.

```c++
      /* Install Forwarding Strategy: best-strategy */
      ndn::StrategyChoiceHelper::InstallAll("/", 
			"/localhost/nfd/strategy/best-strategy");
```

### Istruzioni finali

Viene infine impostata la durata della simulazione, nonché vengono dichiarate le
istruzioni di avvio e di stop.

```c++
      /* Stop simulation after 20.0 sec */
      Simulator::Stop(Seconds(20.0));

      /* Run and destroy simulator when it finish */
      Simulator::Run();
      Simulator::Destroy();
```

### 3.6 Avviare la simulazione

Una volta completata la fase di realizzazione (ovvero scrittura di topologia,
applicazioni e scenario) è possibile avviare la simulazione dello scenario.
Il simulatore è dotato di un visualizzatore grafico che permette di mostrare la
topologia e le comunicazioni tra i nodi.

```bash
NS_LOG=ndn.Producer:ndn.Consumer ./waf --run scenario_name
NS_LOG=ndn.Producer:ndn.Consumer ./waf --run scenario_name --vis
```

Si consulti l'Appendice A contenente le istruzioni complete per la corretta
esecuzione della simulazione.

### 3.7 Logging

Utilizzando la variabile `NS_LOG` fornita al simulatore è possibile specificare
di quali tipologie di host si desidera ottenere un log (stampato su console)
delle azioni effettuate.

Per il collezionamento di altre statistiche si rimanda al relativo Appendice.

### Note di sviluppo

La divisione dei compiti ha permesso di avanzare con il progetto su tutti i
fronti: dal setup del simulatore alla creazione dello scenario fino alla
collezione dei dati finali.

In particolare si evidenzia l'analisi e lo sviluppo separatamente di scenari
intermedi per la comprensione di CS (a cura di Fabrizio Margotta) e di PIT (a
cura di Andrea Casadei).

## 4. Commenti finali

Il progetto appena descritto ha chiaramente punti di espansioni future.

Molti degli elementi utilizzati, a partire dalla topologia, possono essere
ricombinati ad altre simulazioni per l'analisi di ulteriori aspetti di NDN
volutamente trascurati in questo progetto.

Sono possibili analisi prestazionali, analisi su NFD e sul suo comportamento,
implementazione e utilizzo di altri elementi del simulatore, come il plotting di
grafici con il linguaggio R o la produzione di file PCAP.

Si fa notare che il progetto è stata una valida occasione per l'approfondimento
del simulatore stesso, oltre che di NDN.

L'installazione di simulatore e scenario in una macchina Docker ha permesso di
accelerare i tempi di svolgimento del progetto.  
Le macchine virtuali sono validi ambienti di test, tuttavia risultano essere
scomode nel momento in cui bisogna effettuare prove continue nell'installazione
di un software complesso quale ndnSIM.

Sono state altresì utilizzate tutte le _best-practices_ indicate dal team di
sviluppo del simulatore ndnSIM.

Dal punto di vista del design della simulazione è possibile ideare scenari più
avanzati, ovviamente motivati da casi d'uso specifici da analizzare e
documentare.

Riteniamo che la topologia realizzata sia valida per reti piccole e molto
piccole, dal momento in cui il numero di host è relativamente basso. I
collegamenti realizzati sono utili per analizzare contesti logicamente separati.

Dunque, un altro aspetto estendibile può essere il numero di host da
implementare nella simulazione.

Abbiamo tuttavia riscontrato problemi nel comprendere e implementare:
- un numero molto elevato di host, non riproducibile manualmente in un file di
  testo
- un routing path forzato impostando metriche molto elevate per uno specifico
  collegamento

Gli elementi dello sviluppo software dello scenario sono anch'essi estendibili,
è infatti possibile:
- usare nomi differenti per i pacchetti, così come un numero di richieste
  differenti da Consumer a Consumer
- variare la dimensione e l'algoritmo di CS
- preparare ed usare delle FIB manuali
- testare altre Forwarding Strategy, eventualmente creandone di nuove
- ideare e realizzare altri Helpers per Consumer e Producer

Ognuna delle possibili estensioni può essere utile solo e soltanto in scenari
particolarmente ideati.

Per quanto concerne il simulatore, è una scelta praticamente obbligata quella
dell'utilizzo del C++ come linguaggio di implementazione degli scenari e degli
helper. Il simulatore supporta infatti anche il linguaggio Python, tuttavia non
è stata trovata in rete una documentazione sufficiente per interfacciarsi alle
API di ndnSIM attraverso suddetto linguaggio.

Contributi alla community:
- simulatore NDN in docker
- simulazione / progetto segnalato agli sviluppatori

## 5. Appendice A: Guida utente

Come già citato nella relazione tenica, il progetto è stato implementato in una
macchina Docker per permettere la portabilità della simulazione (e del
simulatore), per evitare l'utilizzo di pesanti macchine virtuali e per evitare
l'effetto "works on my machine".

Per ottenere lo scenario è sufficiente eseguire il "build" della macchina
Docker, le cui istruzioni sono contenute nel `Dockerfile` presente nel
repository che ospita tale relazione.

```bash
docker build -t ndnsim .
```

Avviare dunque l'immagine Docker con il comando:

```bash
xhost + && docker run -t -i -h ndn \
    -e DISPLAY=$DISPLAY -v /tmp/.X11-unix/:/tmp/.X11-unix/ ndnsim:latest
```

Docker servirà una shell Bash, da cui si dovrà lanciare il comando
`NS_LOG=ndn.Producer:ndn.Consumer ./waf --run project-scenario` per avviare la
simulazione.

Per ottenere il supporto grafico del simulatore, lanciare
`NS_LOG=ndn.Producer:ndn.Consumer ./waf --run project-scenario --vis` ed avviare
la simulazione con il comando `F3`.

## 6. Appendice B: Introduzione a NDN

Il modello architetturale Internet NDN pone l'Informazione al centro della
comunicazione tra entità. Nel suo dominio esistenziale è centrale il "cosa" - di
interesse delle Applicazioni e degli Utenti - e non il "dove" - di interesse
nell'attuale modello Internet basato su IP, corrisponde a host e indirizzi.

NDN concretizza la comunicazione tra host utilizzando due tipi di pacchetto che
fanno riferimento alla richiesta di una certa informazione (Interest) e alla
risposta contenente (ove presente) l'informazione richiesta (Data).

Interest e Data, al contrario dell'attuale protocollo IP, non fanno più
riferimento a indirizzi dei domini a cui si chiedono (e da cui si ricevono)
informazioni, piuttosto specificano direttamente il Nome della risorsa.

Sarà poi compito dei protocolli di forwarding instradare Interest e Data
attraverso le interfacce dei router che collegano gli host nella rete.

NDN si compone fondamentalmente delle seguenti strutture dati:
- Forwarding Information Base (FIB)
- Pending Interest Table (PIT)
- Content Store (CS)

**spiegare CS e PIT**

## 7. Appendice C: Il simulatore ndnSIM 

ndnSIM è basato sul simulatore ns-3, ma è stato fortemente modificato per essere
adattato alla nuova struttura dei pacchetti utilizzati, ad una diversa tipologia
di host, diverse stutture dati usate dai router, diversi algoritmi di forwarding
e così via.

[inserire foto sul sito di NDNsim]()

Tra le peculiarità del simulatore è bene riportare:
- la scalabilità
- la portabilità degli scenari simulati in contesti fisici
- la possibilità di strutturare la topologia della rete in un file di testo
  separato dalla simulazione
- la presenza di strutture dati avanzate per gestire un insieme di nodi
  contemporaneamente
- la compatibilità con strumenti per il logging e plot di dati su grafici

Il simulatore è installabile seguendo le istruzioni disponibili sul sito. Il
gruppo di lavoro ha realizzato un'immagine Docker che comprende la simulazione
realizzata.

Una simulazione NDN comprende i seguenti elementi:

- topologia della rete
- applicativi
- scenario programmato

### Topologia della rete

Definisce in modo preciso gli host della rete (Consumer, Producer, Router) e i
collegamenti tra essi, comprendendo le velocità di questi ultimi e le latenze.

Può essere definita direttamente nel codice sorgente della simulazione, ma non è
una buona pratica.

Il modo migliore per specificare la topologia è quindi quello di dichiarare gli
host e i collegamenti in un file di testo opportunamente formattato e separato
dal sorgente della simulazione.

Il file contente la topologia è `project-topology.txt` e comprende:
- una prima parte (`router`) in cui vengono dichiarati i nodi che si vogliono
  utilizzare e la posizione in cui si trovano. Il nome del nodo è anche il suo
  identificativo che verrà usato da codice. La scelta dei nomi è arbitraria, ma
  è buona pratica chiamarli a seconda del loro ruolo all'interno della topologia
  per evitare di fare confusione.
- una seconda parte (`link`) in cui si definiscono i collegamenti tra i vari
  nodi, la velocità con cui comunicano, il ritardo tra uno nodo e l'altro e il
  numero massimo di pacchetti che possono rimanere in queue in un collegamento.

### Applicativi

Chiamati anche _helpers_, consistono in piccoli programmi che, utilizzando le
API del simulatore, definiscono il comportamento di Consumer e Producer prima,
durante e dopo l'invio e la ricezione di pacchetti Interest o Data.

Ne esistono di già pronti, messi a disposizione dalla comunità di sviluppatori
NDN, o è possibile scriverne di propri.

### Scenario programmato

Deve definire in modo chiaro e semplice gli attori e le fasi della comunicazione.

Consiste in un piccolo programma scritto in C++ o Python e contiene tutte le
fasi della simulazione, dalla descrizione della topologia della rete
all'installazione degli helpers e dei vari moduli NDN negli host simulati.
