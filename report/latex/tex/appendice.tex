\section{Guida utente} \label{app:guidautente}

Come già citato nella relazione tenica, il progetto è stato implementato in una
macchina Docker per favorire la portabilità della simulazione (e del
simulatore), per evitare l'utilizzo di pesanti macchine virtuali e per evitare
il fenomeno "\textit{works on my machine}".

Per ottenere l'esecuzione dello scenario è sufficiente eseguire il
\textit{build} della macchina Docker ed in seguito lanciarla con gli opportuni
comandi.

\begin{minted}{bash}
git clone https://bitbucket.org/emrevoid-uni/ndn-application.git && \
cd ndn-application/setup/ && \
docker build -t simulation .  
\end{minted}

L'immagine Docker fornita permette di avviare la simulazione dello scenario
appena trattato con il supporto del modulo \textit{visualizer}.

L'utente dovrà quindi digitare il seguente comando:

\begin{minted}{bash}
xhost + && \
docker run \
    -e DISPLAY=$DISPLAY \
    -v /tmp/.X11-unix/:/tmp/.X11-unix/ \
    simulation
\end{minted}

Il modulo visualizer verrà quindi automaticamente lanciato poiché la macchina
Docker eseguirà il seguente comando:

\begin{minted}{bash}
NS_LOG=ndn.Producer:ndn.Consumer ./waf --run project-scenario --vis
\end{minted}

Premere il tasto \texttt{F3} per avviare e fermare la simulazione.

\section{Introduzione a NDN}

Il modello architetturale Internet NDN pone l'Informazione al centro della
comunicazione tra entità.

Nel suo dominio esistenziale è centrale il \textit{cosa} - di interesse delle
Applicazioni e degli Utenti - e non il \textit{dove} - di interesse nell'attuale
modello Internet basato su IP, corrisponde a host e indirizzi.

\begin{figure}[H]
	\centering
	\includegraphics[scale=0.5]{NDN_Architecture_Stack.png}
	\caption{Lo Stack Architetturale NDN comparato all'attuale stack Internet
    basato su IP.}
	\label{fig:ndnstack}
\end{figure}

NDN concretizza la comunicazione tra host utilizzando due tipi di pacchetto che
fanno riferimento:
\begin{itemize}
    \item alla richiesta di una certa informazione (\textit{Interest})
    \item alla risposta contenente (ove presente) l'informazione richiesta
        (\textit{Data})
\end{itemize}

Interest e Data, al contrario dell'attuale protocollo IP, non fanno più
riferimento a indirizzi dei domini a cui si chiedono (e da cui si ricevono)
informazioni, piuttosto specificano direttamente il \textit{Nome} della risorsa.

Sarà poi compito dei protocolli di forwarding instradare Interest e Data
attraverso le interfacce dei router che collegano gli host nella rete.

\begin{figure}[H]
	\centering
	\includegraphics[width=\linewidth]{ndn-packet-format.png}
	\caption{Formato dei pacchetti Interest e Data per NDN.}
	\label{fig:ndnpacketformat}
\end{figure}

I nodi della rete a loro volta assumono una connotazione logica differente:
\begin{itemize}
    \item i \textbf{Consumer} sono coloro che richiedono e consumano una risorsa
        dalla rete
    \item i \textbf{Producer} sono coloro che forniscono le risorse richieste
        dai Consumer
    \item i \textbf{Router} fanno in modo che le richieste e le risposte
        arrivino a destinazione, implementando però meccanismi intelligenti
        (principalmente) di caching per l'ottimizzazione delle prestazioni
\end{itemize}

I router in NDN si compongoono fondamentalmente delle seguenti strutture dati:
\begin{itemize}
    \item Forwarding Information Base (FIB)
    \item Pending Interest Table (PIT)
    \item Content Store (CS)
\end{itemize}

\begin{figure}[H]
	\centering
	\includegraphics[scale=0.5]{ndn-data-structures.png}
	\caption{Strutture dati presenti nei Router NDN e loro workflow
    schematizzato.}
	\label{fig:ndndatastruct}
\end{figure}

La \textbf{Forwarding Information Base} colleziona i prefissi dei \textit{Nomi}
delle risorse disponibili presso una certa interfaccia di rete.

La \textbf{Pending Interest Table} raccoglie le richieste effettuate dai
Consumer e di cui non è ancora giunta una risposta.

\textbf{Content Store} raccoglie i \textit{Data} corrispondenti a richieste già
soddisfatte, effettuando un vero e proprio caching delle risorse che sono
transitate nella rete.

\section{Il simulatore ndnSIM}

ndnSIM è basato sul simulatore ns-3, ma è stato fortemente modificato per essere
adattato alla nuova struttura dei pacchetti utilizzati, ad una diversa tipologia
di host, diverse stutture dati usate dai router, diversi algoritmi di forwarding
e così via.

\begin{figure}[H]
	\centering
	\includegraphics[scale=0.25]{ndnSIM-structure.png}
	\caption{Schema dei componenti che costituiscono e contribuiscono al
    funzionamento del simulatore ndnSIM.}
	\label{fig:ndnsimdstruct}
\end{figure}

Tra le peculiarità del simulatore è bene riportare:
\begin{itemize}
    \item la scalabilità
    \item la portabilità degli scenari simulati in contesti fisici
    \item la possibilità di strutturare la topologia della rete in un file di
        testo separato dalla simulazione
    \item la presenza di strutture dati avanzate per gestire un insieme di nodi
        contemporaneamente
    \item la compatibilità con strumenti per il logging e plot di dati su
        grafici
\end{itemize}

Il simulatore è installabile seguendo le istruzioni disponibili sul sito. Il
gruppo di lavoro ha realizzato un'immagine Docker distribuita in modo libero in
rete \cite{ndnsimdocker,ndnsimdockerhub}.

Una simulazione NDN comprende tipicamente almeno i seguenti elementi: 
\begin{itemize} 
    \item topologia della rete
    \item applicativi
    \item scenario programmato
\end{itemize}

\subsection{Topologia della rete}

Definisce in modo preciso gli host della rete (Consumer, Producer, Router) e i
collegamenti tra essi, comprendendo le velocità di questi ultimi e le latenze.

Può essere definita direttamente nel codice sorgente della simulazione, oppure i
nodi possono essere specificati in un file di testo mediante apposita sintassi.

Il file di testo contentente la topologia della rete da simulare viene così
descritto come segue.

In una prima parte (\texttt{router}) vengono dichiarati i nodi che si vogliono
utilizzare e la posizione in cui si trovano.

Il nome del nodo è anche il suo identificativo che verrà usato da codice. La
scelta dei nomi è arbitraria, ma è buona pratica chiamarli a seconda del loro
ruolo all'interno della topologia per evitare di fare confusione.

Nella seconda parte (\texttt{link}) si definiscono i collegamenti tra i vari
nodi, la velocità con cui comunicano, il ritardo tra uno nodo e l'altro e il
numero massimo di pacchetti che possono rimanere in queue in un collegamento.

\subsection{Applicativi}

Consistono in piccoli programmi che, utilizzando le API del simulatore,
definiscono il comportamento di Consumer e Producer prima, durante e dopo
l'invio e la ricezione di pacchetti Interest o Data.

Ne esistono di già pronti, messi a disposizione dalla comunità di sviluppatori
NDN, o è possibile scriverne di propri.

\subsection{Scenario programmato}

Deve definire in modo chiaro e semplice gli attori e le fasi della
comunicazione.

Consiste in un piccolo programma scritto in C++ o Python e contiene tutte le
fasi della simulazione, dalla descrizione della topologia della rete
all'installazione degli helpers e dei vari moduli NDN negli host simulati.
