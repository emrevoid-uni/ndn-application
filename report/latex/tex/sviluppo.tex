\section{Sviluppo}

\subsection{Workflow con ndnSIM}

Viene di seguito riportato il workflow tipico da adottare in scenari esaminati
con ndnSIM \cite[p.~15]{tutorial}.

\begin{enumerate}
	\item Preparazione dell'ambiente di sviluppo
	\item Definizione della topologia
	\item Configurazione del Content Store
	\item Installazione dello stack NDN nei nodi
	\item Configurazione della Forwarding Information Base
	\item Installazione di una Forwarding Strategy
	\item Esecuzione della simulazione (opzionale: con visualizer)
	\item Raccolta di metriche, statistiche e log
\end{enumerate}

\subsection{Preparazione dell'ambiente di sviluppo}

Il simulatore ndnSIM viene installato in una macchina Docker appositamente
creata dal team, disponibile pubblicamente in rete \cite{ndnsimdocker}.

Lo scenario viene invece sviluppato e fornito in un repository separato da
quello contenente il simulatore \cite{scenariorepo}, seguendo le direttive
fornite dagli sviluppatori \cite{scenariotemplate}.

Adottando le best practices pubblicate dal team di sviluppo di ndnSIM
\cite{ndnsimbestpractices}, di seguito vengono riportati i \textit{commit hash}
dei repository coinvolti nell'attività di progetto:

\begin{itemize}
    \item Patched NS-3 for ndnSIM (\texttt{ns-3}) \cite{ns3repo}:
        \texttt{05158549275e897c7d17922bdeada483b2b8ab11}
    \item PyBindGen (\texttt{pybindgen}) \cite {pybindgenrepo}:
        \texttt{0466ae3508076ccfb803a16c0eeffc2ed3fc32c3}
    \item ndnSIM (\texttt{ndnSIM}) \cite{ndnsimrepo}:
        \texttt{65c490630e1a2e9cdebaf505de8729838c000821}
    \item NDN Forwarding Daemon (\texttt{NFD}) \cite{nfdrepo}:
        \texttt{3bebd1190b1c45f8acaa0fe1d3a3100651a062e4}
    \item NDN C++ library (\texttt{ndn-cxx}) \cite{ndncxxrepo}:
        \texttt{e1ae096efd8ad503ce7dbd616ee174afaed6c66b}
\end{itemize}

\subsection{Definizione della topologia}

La topologia della rete che comprende Router, Producer e Consumer è definita
nell'apposito file di testo (\texttt{project-topology.txt}), opportunamente
importato nel codice sorgente della simulazione utilizzando la classe
\texttt{AnnotatedTopologyReader} \cite{topologyfile}.

Si ricorda che la prima sezione (\texttt{router}) contiene i nodi e la loro
posizione virtuale.
La sezione \texttt{link} descrive i collegamenti e le metriche tra ogni nodo.

Di seguito un estratto del file:

\begin{verbatim}
# project-topology.txt

# router section defines topology nodes and their relative positions
    (e.g., to use in visualizer)
router

# node  comment     yPos    xPos
Con0    NA          1       1
[...]
Con21   NA          4       13
Rtr1    NA          2       2
[...]
Rtr7    NA          5       12
Pro0    NA          6       8

# link section defines point-to-point links between nodes and
    characteristics of these links
link

# srcRtr   dstRtr     bandwidth   metric  delay   queue
Con0        Rtr1       1Mbps       1       10ms    10
[...]
Rtr7        Pro0       1Mbps       1       10ms    10
\end{verbatim}

Si noti che la presenza di un numero di nodi superiore a quelli dichiarati nella
sezione \ref{sec:designtopology} è motivata da possibili usi futuri della
topologia appena mostrata. Inoltre, anche chiamando i nodi \texttt{Con},
\texttt{Pro}, \texttt{Rtr}, facendo rispettivamente riferimento a Consumer,
Producer e Router, essi non sono ancora effettivamente riconosciuti come tali.

Soltanto nel codice sorgente della simulazione si andrà a definire il loro
ruolo.
A tal proposito si rimanda al paragrafo \ref{sec:installndn}.

Di seguito sono riportate le istruzioni necessarie per la lettura del file di
testo all'interno del codice C++ della simualazione.

\begin{minted}{cpp}
/* Reading Topology */
AnnotatedTopologyReader topologyReader("", 25);
topologyReader.SetFileName("./scenarios/project-topology.txt");
topologyReader.Read();
\end{minted}

\subsection{Configurazione del Content Store}

Non è stata effettuata alcuna personalizzazione alla configurazione del Content
Store dei Router, che di default utilizza una policy \textit{Priority-FIFO}
\cite[Chapter 3.3.3 Cache Replacement Policy, p.~23]{csdefault}.

\subsection{Installazione dello stack NDN nei nodi} \label{sec:installndn}

L'installazione dello stack NDN nei nodi desiderati (nello scenario corrente in
tutti) è fondamentale per poter abilitare la comunicazione basata su NDN stesso.

\begin{minted}{cpp}
/* Install NDN stack on all nodes */
ndn::StackHelper ndnHelper;
ndnHelper.InstallAll();
\end{minted}

In seguito va definito il ruolo per ogni nodo, ovvero è necessario specificare
chi è da identificarsi come \textit{Consumer} e chi come \textit{Producer}.

\begin{minted}{cpp}
/* Producer node */
Ptr<Node> producer = Names::Find<Node>("Pro0");

/* Consumer nodes */
NodeContainer coBuildingA;
NodeContainer coBuildingBFloor1;
NodeContainer coBuildingBFloor2;
coBuildingA.Add(Names::Find<Node>("Con16"));
coBuildingBFloor1.Add(Names::Find<Node>("Con1"));
coBuildingBFloor1.Add(Names::Find<Node>("Con8"));
coBuildingBFloor1.Add(Names::Find<Node>("Con9"));
coBuildingBFloor2.Add(Names::Find<Node>("Con2"));
coBuildingBFloor2.Add(Names::Find<Node>("Con3"));
coBuildingBFloor2.Add(Names::Find<Node>("Con10"));
coBuildingBFloor2.Add(Names::Find<Node>("Con11"));
\end{minted}

\subsubsection{Installazione delle applicazioni per i Consumer}

Successivamente è necessario installare un'applicazione (\textit{helper}) nei
Consumer.
Viene inoltre specificato il prefisso del \textit{Nome} trasmesso dai Consumer
all'atto di invio di un Interest.

L'helper scelto è \texttt{ConsumerBatches}, che permette di inviare un numero
arbitrario di Interest dopo un tempo \texttt{T} dall'avvio della simulazione.

Tale comportamento è estremamente utile nello scenario ideato, poiché consente
di installare la stessa strategia nei diversi piani ed edifici.

Come è possibile osservare dal codice che segue, i \textit{batch} sono composti
da una sola richiesta che viene effettuata in momenti differenti.
Ciò consentirà di osservare chiaramente l'utilizzo del Content Store da parte
dei Router per ottenere la risorsa dalla cache.

\begin{minted}{cpp}
/* Set prefix */
std::string prefix = "/prefix";
ndn::AppHelper consumerHelper("ns3::ndn::ConsumerBatches");
consumerHelper.SetPrefix(prefix);

/* Set time strategy on Consumer nodes using Batches */
consumerHelper.SetAttribute("LifeTime", StringValue("5s"));
consumerHelper.SetAttribute("Batches", StringValue("1s 1"));
consumerHelper.Install(coBuildingBFloor1);
consumerHelper.SetAttribute("Batches", StringValue("3s 1"));
consumerHelper.Install(Names::Find<Node>("Con0"));
consumerHelper.SetAttribute("Batches", StringValue("4s 1"));
consumerHelper.Install(coBuildingA);
consumerHelper.SetAttribute("Batches", StringValue("5s 1"));
consumerHelper.Install(coBuildingBFloor2);
\end{minted}

\subsubsection{Installazione delle applicazioni per i Producer}

Allo stesso modo viene installato un \textit{helper} nell'unico Producer
ospitato dallo scenario.
Il comportamento di tale applicativo è molto semplice, in quanto risponde
all'Interest richiesto con un pacchetto \textit{Data} della dimensione
(virtuale) indicata.

\begin{minted}{cpp}
/* Install NDN applications on Producer */
ndn::AppHelper producerHelper("ns3::ndn::Producer");
producerHelper.SetPrefix(prefix);
producerHelper.SetAttribute("PayloadSize", StringValue("1024"));
producerHelper.Install(producer);
\end{minted}

\subsection{Configurazione della Forwarding Information Base}

Utilizziamo il componente \texttt{GlobalRoutingHelper} per calcolare
automaticamente le \textit{routes} e la \textit{Forwarding Information Base}.

\begin{minted}{cpp}
/* Installing global routing interface on all nodes */
ndn::GlobalRoutingHelper ndnGlobalRoutingHelper;
ndnGlobalRoutingHelper.InstallAll();

/* Add `/prefix` origins to ndn::GlobalRouter */
ndnGlobalRoutingHelper.AddOrigins(prefix, producer);

/* Calculate and install FIBs */
ndn::GlobalRoutingHelper::CalculateRoutes();
\end{minted}

\subsection{Installazione di una Forwarding Strategy}

Come forwarding strategy è stata scelta \textit{Best Strategy} poiché inoltra
gli Interest al nodo successivo con costo inferiore.

L'installazione della forwarding strategy avviene mediante
\texttt{StrategyChoiceHelper}.

\begin{minted}{cpp}
/* Install Forwarding Strategy: best-strategy */
ndn::StrategyChoiceHelper::InstallAll("/",
    "/localhost/nfd/strategy/best-strategy");
\end{minted}

\subsection{Istruzioni finali}

Viene infine impostata la durata della simulazione, nonché vengono chiamate le
istruzioni di avvio e di stop.

\begin{minted}{cpp}
/* Stop simulation after 20.0 sec */
Simulator::Stop(Seconds(20.0));

/* Run and destroy simulator when it finish */
Simulator::Run();
Simulator::Destroy();
\end{minted}

\subsection{Esecuzione della simulazione}

Una volta completata la fase di realizzazione (ovvero scrittura di topologia,
applicazioni e scenario) è possibile avviare la simulazione dello scenario.

Il simulatore è dotato di un visualizzatore grafico che permette di mostrare la
topologia e le comunicazioni tra i nodi ed è attivabile con l'opzione
\texttt{--vis}.

\begin{minted}{bash}
NS_LOG=ndn.Producer:ndn.Consumer ./waf --run scenario_name
NS_LOG=ndn.Producer:ndn.Consumer ./waf --run scenario_name --vis
\end{minted}

Si consulti l'Appendice \ref{app:guidautente} contenente le istruzioni complete
per la corretta esecuzione della simulazione.

\subsection{Raccolta di metriche, statistiche e log}

Utilizzando la variabile \texttt{NS\_LOG}, fornita al simulatore, è possibile
specificare quali tipologie di host sono abilitati a stampare su console un log 
delle azioni effettuate.

La raccolta dei risultati della simulazione può essere effettuata con i
\textit{trace helpers}, che permettono di ottenere metriche sui pacchetti, sul
Content Store e sulle Applicazioni \cite{metricsdoc}.

\subsection{Note di sviluppo}

La divisione dei compiti ha permesso di avanzare con il progetto su tutti i
fronti: dal setup del simulatore alla creazione dello scenario fino alla
collezione dei dati finali.

In particolare si evidenzia l'analisi e lo sviluppo separatamente di scenari
intermedi per la comprensione di Content Store (a cura di Fabrizio Margotta) e
di Pending Interest Table (a cura di Andrea Casadei).
