#!/bin/bash

counter=0
width=650
height=450

for IMG in $(ls *.png)
do
    counter=$((counter+1))
    convert ${IMG} -crop ${width}x${height}+700+30 out/${counter}_s.png 
done
