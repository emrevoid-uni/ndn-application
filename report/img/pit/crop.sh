#!/bin/bash

counter=0

for IMG in $(ls *.png)
do
    counter=$((counter+1))
    convert ${IMG} -crop 600x300+20+35 ${counter}_a.png
    convert ${IMG} -crop 600x300+20+381 ${counter}_b.png
    convert ${counter}_a.png ${counter}_b.png -append ${counter}_p.png
    cp ${counter}_p.png ../real
done


