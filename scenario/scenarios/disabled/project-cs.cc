// ndn-grid-topo-plugin.cpp

#include "ns3/core-module.h"
#include "ns3/network-module.h"
#include "ns3/ndnSIM-module.h"

namespace ns3 {

int
main(int argc, char* argv[])
{
  CommandLine cmd;
  cmd.Parse(argc, argv);

  // Reading topology
  AnnotatedTopologyReader topologyReader("", 25);
  topologyReader.SetFileName("./scenarios/project-topo.txt");
  topologyReader.Read();

  // Install NDN stack on all nodes
  ndn::StackHelper ndnHelper;
  // ndnHelper.SetOldContentStore("ns3::ndn::cs::Lru", "MaxSize", "100"); // default ContentStore parameters (OldContentStore needed to get metrics work)
  ndnHelper.InstallAll();

  // Install Forwaring Strategy: BestRoute
  ndn::StrategyChoiceHelper::InstallAll("/", "/localhost/nfd/strategy/best-strategy");

  // Installing global routing interface on all nodes
  ndn::GlobalRoutingHelper ndnGlobalRoutingHelper;
  ndnGlobalRoutingHelper.InstallAll();

  // Getting containers for the consumer/producer
  Ptr<Node> producer = Names::Find<Node>("Pro0");
  NodeContainer coBuildingA;
  NodeContainer coBuildingBFloor1;
  NodeContainer coBuildingBFloor2;
  coBuildingA.Add(Names::Find<Node>("Con16"));
  coBuildingBFloor1.Add(Names::Find<Node>("Con1"));
  coBuildingBFloor1.Add(Names::Find<Node>("Con8"));
  coBuildingBFloor1.Add(Names::Find<Node>("Con9"));
  coBuildingBFloor2.Add(Names::Find<Node>("Con2"));
  coBuildingBFloor2.Add(Names::Find<Node>("Con3"));
  coBuildingBFloor2.Add(Names::Find<Node>("Con10"));
  coBuildingBFloor2.Add(Names::Find<Node>("Con11"));

  // Install NDN applications
  std::string prefix = "/prefix";

  ndn::AppHelper consumerHelper("ns3::ndn::ConsumerBatches");
  consumerHelper.SetPrefix(prefix);
  consumerHelper.SetAttribute("LifeTime", StringValue("5s"));
  consumerHelper.SetAttribute("Batches", StringValue("1s 1"));
  consumerHelper.Install(coBuildingBFloor1);
  consumerHelper.SetAttribute("Batches", StringValue("3s 1"));
  consumerHelper.Install(Names::Find<Node>("Con0"));
  consumerHelper.SetAttribute("Batches", StringValue("4s 1"));
  consumerHelper.Install(coBuildingA);
  consumerHelper.SetAttribute("Batches", StringValue("5s 1"));
  consumerHelper.Install(coBuildingBFloor2);

  ndn::AppHelper producerHelper("ns3::ndn::Producer");
  producerHelper.SetPrefix(prefix);
  producerHelper.SetAttribute("PayloadSize", StringValue("1024"));
  producerHelper.Install(producer);

  // Add /prefix origins to ndn::GlobalRouter
  ndnGlobalRoutingHelper.AddOrigins(prefix, producer);

  // Calculate and install FIBs
  ndn::GlobalRoutingHelper::CalculateRoutes();

  Simulator::Stop(Seconds(20.0));

  // Install CS Tracer
  //ndn::CsTracer::InstallAll("cs-trace.txt", Seconds(1));

  Simulator::Run();
  Simulator::Destroy();

  return 0;
}

} // namespace ns3

int
main(int argc, char* argv[])
{
  return ns3::main(argc, argv);
}
