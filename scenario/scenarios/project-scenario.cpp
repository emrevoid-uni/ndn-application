/*
 * Author:
 * - Casadei Andrea <andrea.casadei22@studio.unibo.it>
 * - Margotta Fabrizio <fabrizio.margotta@studio.unibo.it>
 *
 * This scenario was created for a project with Professor Giovanni Pau.
 * His goal is to give us a better understanding of how PIT and Content-Store works,
 * we also made our custom topology to recreate the scenario of a real building.
 */

#include "ns3/core-module.h"
#include "ns3/network-module.h"
#include "ns3/ndnSIM-module.h"

namespace ns3 {
    int main(int argc, char* argv[])
    {
      CommandLine cmd;
      cmd.Parse(argc, argv);

      /* Reading Topology */
      AnnotatedTopologyReader topologyReader("", 25);
      topologyReader.SetFileName("./scenarios/project-topology.txt");
      topologyReader.Read();

      /* Install NDN stack on all nodes */
      ndn::StackHelper ndnHelper;
      ndnHelper.InstallAll();

	  /* Getting containers for the consumer/producer */
      Ptr<Node> producer = Names::Find<Node>("Pro0");

      /* Create consumerNode */
      NodeContainer coBuildingA;
      NodeContainer coBuildingBFloor1;
      NodeContainer coBuildingBFloor2;
      coBuildingA.Add(Names::Find<Node>("Con16"));
      coBuildingBFloor1.Add(Names::Find<Node>("Con1"));
      coBuildingBFloor1.Add(Names::Find<Node>("Con8"));
      coBuildingBFloor1.Add(Names::Find<Node>("Con9"));
      coBuildingBFloor2.Add(Names::Find<Node>("Con2"));
      coBuildingBFloor2.Add(Names::Find<Node>("Con3"));
      coBuildingBFloor2.Add(Names::Find<Node>("Con10"));
      coBuildingBFloor2.Add(Names::Find<Node>("Con11"));

      /* Install NDN applications */
      std::string prefix = "/prefix";
      ndn::AppHelper consumerHelper("ns3::ndn::ConsumerBatches");
      consumerHelper.SetPrefix(prefix);

      /* Install NDN applications on consumerNode */
      consumerHelper.SetAttribute("LifeTime", StringValue("5s"));
      consumerHelper.SetAttribute("Batches", StringValue("1s 1"));
      consumerHelper.Install(coBuildingBFloor1);
      consumerHelper.SetAttribute("Batches", StringValue("3s 1"));
      consumerHelper.Install(Names::Find<Node>("Con0"));
      consumerHelper.SetAttribute("Batches", StringValue("4s 1"));
      consumerHelper.Install(coBuildingA);
      consumerHelper.SetAttribute("Batches", StringValue("5s 1"));
      consumerHelper.Install(coBuildingBFloor2);

      /* Install NDN applications on Producer */
      ndn::AppHelper producerHelper("ns3::ndn::Producer");
      producerHelper.SetPrefix(prefix);
      producerHelper.SetAttribute("PayloadSize", StringValue("1024"));
      producerHelper.Install(producer);
      
	  /* Installing global routing interface on all nodes */
      ndn::GlobalRoutingHelper ndnGlobalRoutingHelper;
      ndnGlobalRoutingHelper.InstallAll();
      
	  /* Add `/prefix` origins to ndn::GlobalRouter */
      ndnGlobalRoutingHelper.AddOrigins(prefix, producer);

      /* Calculate and install FIBs */
      ndn::GlobalRoutingHelper::CalculateRoutes();
      
	  /* Install Forwarding Strategy: best-strategy */
      ndn::StrategyChoiceHelper::InstallAll("/", "/localhost/nfd/strategy/best-strategy"); 

      /* Stop simulation after 20.0 sec */
      Simulator::Stop(Seconds(20.0));

      /* Run and destroy simulator when it finish */
      Simulator::Run();
      Simulator::Destroy();

      return 0;
    }
} /* namespace ns3 */

int main(int argc, char* argv[])
{
  return ns3::main(argc, argv);
}
