# Network Programming project: NDN

Project for Network Programming Course. University of Bologna, Computer Science
and Engineering.

Authors: Andrea Casadei and Fabrizio Margotta

## Repository

- `proposal`: project proposal (italian) for professors
- `setup`: Dockerfiles for scenario installation
- `scenario`: NDN scenario for our project (uses the scenario template folder
  taken from [NDN repos](https://github.com/named-data-ndnSIM/scenario-template))
- `report`: project report (italian) for professors, students and everyone who
  wants to go further with our work

## Installation

Build the provided `Dockerfile` in the `setup` folder:

```
git clone https://bitbucket.org/emrevoid-uni/ndn-application.git && \
cd ndn-application/setup/ && \
docker build -t simulation .
```

It uses the ndnSIM docker image (version 2.5) deployed by us to Docker Hub:
[emrevoid/ndnsim](https://hub.docker.com/r/emrevoid/ndnsim).

## Run

**Note**: the command `xhost +` is needed if you want to use the visualizer.

```
xhost + && \
docker run \
    -e DISPLAY=$DISPLAY \
    -v /tmp/.X11-unix/:/tmp/.X11-unix/ \
    simulation
```

Then press the `F3` button to start/stop the simulation.

If you edit the entrypoint to spawn a shell and want to manually start a
simulation, please run the following command into the docker machine:

```
NS_LOG=ndn.Producer:ndn.Consumer ./waf --run project-scenario
```

Note:

- to use the visualizer, append the `--vis` option to the `waf` command
- `.cpp` file extension is missing!
- logging is provided to `waf` using the `NS_LOG` variable, it should be
  interesting to log the Consumer(s) and the Producer(s) using `ndn.Consumer`
	and `ndn.Producer`

## The project

Our project aim to study the behavior of PIT (Pending Interest Table) and CS
(Content Store) in a small and simulated NDN network.

These two components can be used to provide multipath forwarding to the various
Consumers.

We also provide a topology which can be used in your simulation scenarios: it is
composed of 22 Consumers, 1 Producer and 8 Routers.

It is a layered topology, which can be helpful to study scenarios with multipath
forwarding.

![](./report/img/topology.png)

Our scenario uses a subpart of latter topology:

![](./report/res/topology.png)
