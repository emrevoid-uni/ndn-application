# Proposal

Generate PDF with `pandoc`:

```
pandoc -f gfm -t latex -o proposal.pdf proposal.md
```
