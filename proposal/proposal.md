# NDN Application

Casadei Andrea, Margotta Fabrizio

## Description

Il gruppo si pone l'obiettivo di sviluppare un'applicazione che permetta di
analizzare le caratteristiche introduttive di NDN.

Abbiamo individuato nell'architettura di NDN quattro elementi fondanti:
- Forwarding Strategy
- Content Store
- Pending Interest Table
- Forwarding Information Base

ognuno dei quali svolge e contribuisce, con gli altri elementi, al corretto
funzonamento di una comunicazione di rete basata su NDN.

Abbiamo dunque posto particolare attenzione all'analisi degli elementi _Content Store_ e
_Pending Interest Table_.

Il progetto, di cui in seguito la spiegazione, vuole definire uno scenario
d'utilizzo degli aspetti sopracitati, fornendo design e prototipo definiti con
il docente.

## Applicazione

Il team si focalizzerà nello sviluppo di semplici moduli di applicazione che
permettano la comunicazione tra le entità di una rete basata su NDN, ovvero
Consumer, Producer e Router.

Lo scenario ideato consiste nella comunicazione tra gli host precedentemente
indicati, con l'obiettivo di analizzare le caratteristiche di comunicazione 
di interesse: il caching e il multipath forwarding.

In ambiente simulato, quindi, si provvederà all'esecuzione di tali moduli e
alla loro comunicazione.

L'analisi delle funzionalità citate verranno effettuate mandando più
richieste con lo stesso "Name" da Consumer diversi allo stesso Router.
In tal modo sarà possibile sfruttare il multipath forwarding.
Per sfruttare il caching adoperato dai Router NDN verranno effettuate le stesse
richieste in tempi differenti.

## Funzionalità minime ritenute obligatorie:

- sviluppo in ambiente simulato
- sviluppo di almeno due consumer, un producer e un router
- adozione di un buon design che soddisfi e risolva lo scenario ideato
- creazione di un buon prototipo software, manutenibile e riutilizzabile

## Funzionalità opzionali:

- adozione di tecniche di programmazione efficiente
- adozione del visualizzatore fornito con NDNsim
- creazione di un prototipo fisico

## Challange principali previste:

- comprensione basilare di NDN
- analisi delle funzionalità architetturali di Content Store per il caching e
di un utilizzo smart della PIT per effettuare multipath forwarding
- approccio introduttivo al simulatore NDN
